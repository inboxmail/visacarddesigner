import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';


// class textItem {
//   item: string = "";
//   focus: boolean = false;
//   distanceFromLeft: number = 0;
//   distanceFromBottom: number = 0;
// }

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('flipState', [
      state('active', style({
        transform: 'rotateY(180deg)'
      })),
      state('inactive', style({
        transform: 'rotateY(0)'
      })),
      transition('active => inactive', animate('400ms ease-out')),
      transition('inactive => active', animate('400ms ease-in'))
    ])
  ]
})



export class AppComponent {
  title = 'carddesign';
  @ViewChild('text')
  textId!: ElementRef;
  frontSide: boolean = true;
  text: string = "";
  btnStr: string = "Move to Back"
  distanceFromLeft: number = 0;
  distanceFromBottom: number = 0;
  cardName: string = "BANK NAME";
  cardNameDfl: number = 0;
  cardNameDfb: number = 30;
  cardNumber: string = "4444 4444 4444 4444";
  cardNumberDfl: number = 0;
  cardNumberDfb: number = 0;
  cardExpiryDate: string = "12/25";
  cardExpiryDateDfl: number = 0;
  cardExpiryDateDfb: number = 0;
  userName: string = "USER NAME";
  userNameDfl: number = 0;
  userNameDfb: number = 0;
  currentSelected: any = "";
  cvv: string = "123";

  url: any = "./../assets/defaultBgImageFront.png";
  bgFronturl: any = "./../assets/defaultBgImageFront.png";
  bgBackturl: any = "./../assets/defaultBgImageBack.jpeg";
  iconurl: any = "./../assets/icon.png";
  msg = "";

  flip: string = 'inactive';
  cardCVVDfb: number = 0;
  cardCVVDfl: number = 0;
  helpingText:string = 'Enter some helping text here.';

  cardNameTop: string = '120px';

  cardNumPosition = { x: 40, y: 150 };
  possOnDrag: [] = [];
  cardNumWidth: string = '300px';
  cardNamWidth: string = '150px';
  expirPosition = { x: 200, y: 180 };
  cardNamePosition = { x: 320, y: 40 };
  userNamePosition = { x: 40, y: 200 };
  logoPosition = { x: 20, y: -40 };
  cvvPosition = { x: 350, y: 15 };
  defaultfont = "Arial";
  public frontElements: Array<any> = ['cardNameV', 'cardNumberV', 'userNameV', 'cardExpiryDateV']
  public backElements: Array<any> = ['cvvV']
  public fontfamilys: Array<any> = [
    { 'title': 'Georgia', 'value': 'Georgia, serif' },
    { 'title': 'Times New Roman', 'value': 'Times New Roman' },
    { 'title': 'Palatino Linotype', 'value': 'Palatino Linotype' },
    { 'title': 'Arial', 'value': 'Arial, sans-serif' },
    { 'title': 'Verdana', 'value': 'Verdana, sans-serif' },
    { 'title': 'Helvetica', 'value': 'Helvetica, sans-serif' },
    { 'title': 'Tahoma', 'value': 'Tahoma, sans-serif' },
    { 'title': 'Trebuchet MS', 'value': 'Trebuchet MS, sans-serif' },
    { 'title': 'Garamond', 'value': 'Garamond, sans-serif' },
    { 'title': 'Courier New', 'value': 'Courier New, monospace' },
    { 'title': 'Brush Script MT', 'value': 'Brush Script MT, cursive' },
  ];

  //https://stackblitz.com/edit/angular-image-resize
  onClick(event) {
    this.currentSelected = event.target.value
    if (this.frontElements.includes(this.getIdOfCurrentElement())) {
      this.btnStr = "Move to back"
      document.getElementById('helpingtext')!.style.display = "block";

    }
    else {
      this.btnStr = "Move to Front";
      document.getElementById('helpingtext')!.style.display = "none";
    }
    if (this.frontSide && this.backElements.includes(this.getIdOfCurrentElement()))
      this.toggleFlip()
    else if (!this.frontSide && this.frontElements.includes(this.getIdOfCurrentElement()))
      this.toggleFlip()

    if (event.target.value == '1') {
      this.text = this.cardName
      this.distanceFromBottom = this.cardNamePosition.x;
      this.distanceFromLeft = this.cardNamePosition.y;
    }
    else if (event.target.value == '2') {
      this.text = this.cardNumber
      this.distanceFromBottom = this.cardNumPosition.x;
      this.distanceFromLeft = this.cardNumPosition.y;
    }
    else if (event.target.value == '3') {
      this.text = this.userName
      this.distanceFromBottom = this.userNamePosition.x;
      this.distanceFromLeft = this.userNamePosition.y;
    }
    else if (event.target.value == '4') {
      this.text = this.cardExpiryDate
      this.distanceFromBottom = this.expirPosition.x;
      this.distanceFromLeft = this.expirPosition.y;
    }
    else if (event.target.value == '5') {
      this.text = this.cvv
      this.distanceFromBottom = this.cardCVVDfb
      this.distanceFromLeft = this.cardCVVDfl
    }
    //console.log(event)
  }

  toggleFlip() {
    this.flip = (this.flip == 'inactive') ? 'active' : 'inactive';
    this.frontSide = this.frontSide ? false : true;
    if (this.url == this.bgFronturl) {
      this.url = this.bgBackturl
      this.backElements.forEach(element => {
        document.getElementById(element)!.style.display = "block";
      });
      this.frontElements.forEach(element => {
        document.getElementById(element)!.style.display = "none";
      });
    }
    else {
      this.backElements.forEach(element => {
        document.getElementById(element)!.style.display = "none";
      });
      this.frontElements.forEach(element => {
        document.getElementById(element)!.style.display = "block";
      });
      this.url = this.bgFronturl;

    }
    if (this.frontElements.includes(this.getIdOfCurrentElement())) {
      this.btnStr = "Move to back"
      document.getElementById('helpingtext')!.style.display = "block";
    }
    else {
      this.btnStr = "Move to Front"
      document.getElementById('helpingtext')!.style.display = "none";
    }

  }

  updateValues(event) {
    //console.log(event);
    if (this.currentSelected == '1') {
      this.cardName = this.text
    }
    else if (this.currentSelected == '2') {
      this.cardNumber = this.text
    }
    else if (this.currentSelected == '3') {
      this.userName = this.text
    }
    else if (this.currentSelected == '4') {
      this.cardExpiryDate = this.text
    }
    else if (this.currentSelected == '5') {
      this.cvv = this.text
    }
  }

  getIdOfCurrentElement() {
    switch (this.currentSelected) {
      case '1':
        return 'cardNameV';
        break;
      case '2':
        return 'cardNumberV';
      case '3':
        return 'userNameV';
      case '4':
        return 'cardExpiryDateV';
      case '5':
        return 'cvvV';
    }
    return 'none';
  }
  changeSide() {
    var element = this.getIdOfCurrentElement();

    if (element != 'none') {
      if (this.frontElements.includes(element)) {
        this.backElements.push(element);
        // if (this.frontElements.indexOf(element) !== -1)
        this.frontElements.splice(this.frontElements.indexOf(element), 1);
      }
      else {
        this.frontElements.push(element);
        //if (this.backElements.indexOf(element) !== -1)
        this.backElements.splice(this.backElements.indexOf(element), 1);
      }
      //console.log('front side : ' + this.frontElements)
      //console.log('back side : ' + this.backElements)
      this.toggleFlip();

      if (this.backElements.includes(this.getIdOfCurrentElement())) {
        if (this.frontSide)
          this.toggleFlip()
      }
      else {
        if (!this.frontSide)
          this.toggleFlip()
      }


    }


  }

  OnDragEnded(event) {
    // console.log('root ==',event.source.getRootElement() );
    var parentPos = document.getElementById('mydiv')!.getBoundingClientRect();
    var childPos = document.getElementById(this.getIdOfCurrentElement())!.getBoundingClientRect();

    // console.log("top : " + (childPos.top - parentPos.top))
    // console.log("right : " + (childPos.right - parentPos.right))
    // console.log("bottom : " + (childPos.bottom - parentPos.bottom))
    // console.log("left : " + (childPos.left - parentPos.left))

    this.possOnDrag = event.source.getRootElement().getBoundingClientRect();
    this.distanceFromBottom = childPos.top - parentPos.top;
    this.distanceFromLeft = childPos.left - parentPos.left

  }
  onFontClick(event) {
    this.defaultfont = event.target.value;
    //console.log(this.defaultfont);
    document.getElementById(this.getIdOfCurrentElement())!.style.fontFamily = this.defaultfont;
  }

  selectFile(event: any, id: any) {
    if (!event.target.files[0] || event.target.files[0].length == 0) {
      this.msg = 'You must select an image';
      return;
    }
    var mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.msg = "Only images are supported";
      return;
    }
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event) => {
      this.msg = "";
      if (id == 'cardIcon')
        this.iconurl = reader.result;
      else if (id == 'bgImageFront') {
        this.url = reader.result;
        if (!this.frontSide) this.toggleFlip()
        this.bgFronturl = reader.result;
      }
      else if (id == 'bgImageBack') {
        this.url = reader.result;
        if (this.frontSide) this.toggleFlip()
        this.bgBackturl = reader.result;
      }

    }
  }
}

